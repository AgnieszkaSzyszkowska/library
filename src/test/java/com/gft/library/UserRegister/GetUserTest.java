package com.gft.library.UserRegister;

import com.gft.library.SpringBootWebApplication;
import com.gft.library.UserRegister.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SpringBootWebApplication.class)
@TestPropertySource(locations = "classpath:config/application-integrationtest.properties")
public class GetUserTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getUser(){

        ResponseEntity<User> responseEntity = restTemplate.getForEntity("/user/id?id=1", User.class);

        User user = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Grzegorz", user.getUserName());
    }




}
