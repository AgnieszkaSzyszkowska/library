package com.gft.library.UserRegister;

import com.gft.library.SpringBootWebApplication;
import com.gft.library.UserRegister.model.User;
import com.gft.library.UserRegister.service.dto.UserDTO;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SpringBootWebApplication.class)
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:config/application-integrationtest.properties")
public class AddUserTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void addUser(){

        UserDTO userDTO = new UserDTO();
        userDTO.setUserName("Grzegorz");
        userDTO.setUserSurname("Brzęczyszczykiewicz");
        userDTO.setUserRole("USER");

        ResponseEntity<User> responseEntity = restTemplate.postForEntity("/user/new", userDTO, User.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
