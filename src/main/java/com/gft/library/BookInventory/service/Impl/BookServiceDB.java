package com.gft.library.BookInventory.service.Impl;

import com.gft.library.BookInventory.model.Book;
import com.gft.library.BookInventory.mapper.AuthorMapper;
import com.gft.library.BookInventory.mapper.BookMapper;
import com.gft.library.BookInventory.repository.AuthorRepository;
import com.gft.library.BookInventory.repository.BookRepository;
import com.gft.library.BookInventory.service.BookService;
import com.gft.library.BookInventory.service.dto.AuthorDTO;
import com.gft.library.BookInventory.service.dto.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceDB implements BookService{

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private AuthorMapper authorMapper;
    private BookMapper bookMapper;

    @Autowired
    public BookServiceDB(final AuthorRepository authorRepository, final BookRepository bookRepository, final AuthorMapper authorMapper, final BookMapper bookMapper) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.authorMapper = authorMapper;
        this.bookMapper = bookMapper;
    }

    @Override
    public void addBook(BookDTO bookDTO) {
        bookRepository.save(bookMapper.mapBookDtoToBookDao(bookDTO));
    }

    @Override
    public void deleteBook(Long id) {
        bookRepository.delete(id);
    }

    @Override
    public BookDTO findById(Long id) {
        return bookMapper.mapBookDaoToBookDto(bookRepository.findOne(id));
    }

    @Override
    public List<BookDTO> findByTitle(String title) {
        List<BookDTO> bookDTOS = new ArrayList<>();
        List<Book> books = bookRepository.findByBookTitle(title);
        for(Book b: books){
            bookDTOS.add(bookMapper.mapBookDaoToBookDto(b));
        }
        return bookDTOS;
    }

    @Override
    public List<BookDTO> findByAuthor(AuthorDTO authorDTO) {
        List<BookDTO> bookDTOS = new ArrayList<>();
        List<Book> books = bookRepository.findByBookAuthors(authorMapper.mapAuthorDtoToAuthorDao(authorDTO));
        for(Book b: books){
            bookDTOS.add(bookMapper.mapBookDaoToBookDto(b));
        }
        return bookDTOS;
    }
}
