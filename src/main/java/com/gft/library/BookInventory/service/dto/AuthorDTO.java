package com.gft.library.BookInventory.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthorDTO {

    private Long authorId;

    private String authorName;

    private String AuthorSurname;

}
