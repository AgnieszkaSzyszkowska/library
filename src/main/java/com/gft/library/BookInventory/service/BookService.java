package com.gft.library.BookInventory.service;

import com.gft.library.BookInventory.service.dto.AuthorDTO;
import com.gft.library.BookInventory.service.dto.BookDTO;

import java.util.List;

public interface BookService {

    void addBook(BookDTO bookDTO);
    void deleteBook(Long id);
    BookDTO findById(Long id);
    List<BookDTO> findByTitle(String title);
    List<BookDTO> findByAuthor(AuthorDTO authorDTO);
}
