package com.gft.library.BookInventory.service.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class BookDTO {

    private Long bookId;

    private String bookTitle;

    private List<AuthorDTO> bookAuthors;

}
