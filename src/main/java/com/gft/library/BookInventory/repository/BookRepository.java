package com.gft.library.BookInventory.repository;

import com.gft.library.BookInventory.model.Author;
import com.gft.library.BookInventory.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, Long>, JpaRepository<Book, Long> {
    List<Book> findByBookAuthors(Author author);

    List<Book> findByBookTitle(String title);

}
