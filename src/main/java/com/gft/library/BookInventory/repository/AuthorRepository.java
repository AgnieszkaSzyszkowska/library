package com.gft.library.BookInventory.repository;

import com.gft.library.BookInventory.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long>, JpaRepository<Author, Long> {
}
