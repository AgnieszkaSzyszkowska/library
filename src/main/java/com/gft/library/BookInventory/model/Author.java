package com.gft.library.BookInventory.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "author")
@Data
@NoArgsConstructor
public class Author {

    @Id
    @GenericGenerator(
            name = "author_sequence",
            strategy = "sequence",
            parameters = {
                    @org.hibernate.annotations.Parameter(
                            name = "sequence",
                            value = "author_sequence"
                    )

            })
    @GeneratedValue(generator = "author_sequence")
    @Column(name = "author_id", nullable = false)
    private Long authorId;

    @Column(name = "author_name", nullable = false)
    private String authorName;

    @Column(name = "author_surname", nullable = false)
    private String AuthorSurname;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "bookAuthors")
    private List<Book> books = new ArrayList<>();
}
