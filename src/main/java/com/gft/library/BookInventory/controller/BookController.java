package com.gft.library.BookInventory.controller;

import com.gft.library.BookInventory.service.BookService;
import com.gft.library.BookInventory.service.dto.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = "application/json")
    public void addBook (@RequestBody BookDTO bookDTO){
        bookService.addBook(bookDTO);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public void deleteBook (@RequestParam(value = "id") Long id){
        bookService.deleteBook(id);
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET, produces = "application/json")
    public BookDTO findUser(@RequestParam(value = "id") Long id){
        return bookService.findById(id);
    }

    @RequestMapping(value = "/name", method = RequestMethod.GET, produces = "application/json")
    public List<BookDTO> findUserByName(@RequestParam(value = "name") String name){
        return bookService.findByTitle(name);
    }



}
