package com.gft.library.BookInventory.mapper;

import com.gft.library.BookInventory.model.Author;
import com.gft.library.BookInventory.model.Book;
import com.gft.library.BookInventory.service.dto.AuthorDTO;
import com.gft.library.BookInventory.service.dto.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookMapper {

    private AuthorMapper authorMapper;

    @Autowired
    public BookMapper(AuthorMapper authorMapper) {
        this.authorMapper = authorMapper;
    }

    public Book mapBookDtoToBookDao(BookDTO bookDTO){
        Book book = new Book();
        book.setBookTitle(bookDTO.getBookTitle());
        List<Author> authors = new ArrayList<>();
        for(AuthorDTO a: bookDTO.getBookAuthors()){
            authors.add(authorMapper.mapAuthorDtoToAuthorDao(a));
        }
        book.setBookAuthors(authors);

        return book;
    }

    public BookDTO mapBookDaoToBookDto(Book book){
        BookDTO bookDTO = new BookDTO();
        bookDTO.setBookId(book.getBookId());
        bookDTO.setBookTitle(book.getBookTitle());
        List<AuthorDTO> authorDTOS = new ArrayList<>();
        for(Author a: book.getBookAuthors()){
            authorDTOS.add(authorMapper.mapAuthorDaoToAuthorDto(a));
        }
        bookDTO.setBookAuthors(authorDTOS);

        return bookDTO;
    }
}
