package com.gft.library.BookInventory.mapper;

import com.gft.library.BookInventory.model.Author;
import com.gft.library.BookInventory.service.dto.AuthorDTO;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapper {

    public Author mapAuthorDtoToAuthorDao(AuthorDTO authorDTO){
        Author author = new Author();
        author.setAuthorName(authorDTO.getAuthorName());
        author.setAuthorSurname(authorDTO.getAuthorSurname());

        return author;
    }

    public AuthorDTO mapAuthorDaoToAuthorDto(Author author){
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorId(author.getAuthorId());
        authorDTO.setAuthorName(author.getAuthorName());
        authorDTO.setAuthorSurname(author.getAuthorSurname());

        return authorDTO;
    }
}
