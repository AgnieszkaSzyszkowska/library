package com.gft.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class SpringBootWebApplication {

    public static void main(String[] args) throws Exception{
        SpringApplication.run(SpringBootWebApplication.class, args);
    }
}
