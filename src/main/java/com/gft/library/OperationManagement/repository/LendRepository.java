package com.gft.library.OperationManagement.repository;

import com.gft.library.OperationManagement.model.LendHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Date;

public interface LendRepository extends CrudRepository<LendHistory, Long>, JpaRepository<LendHistory, Long> {

//    @Query("UPDATE lend_history SET return_date = ':returnDate' where bookId = :bookId")
//    void returnBook(@Param("bookId") Long bookId, @Param("returnDate") Date returnDate);
}
