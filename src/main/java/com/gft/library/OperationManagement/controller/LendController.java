package com.gft.library.OperationManagement.controller;

import com.gft.library.OperationManagement.service.LendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/book")
public class LendController {

    private LendService lendService;

    @Autowired
    public LendController(LendService lendService) {
        this.lendService = lendService;
    }

    @RequestMapping(value = "/lend", method = RequestMethod.PATCH, consumes = "application/json")
    public void lendBook(@RequestParam(value = "bookId") Long bookId, @RequestParam(value = "userId") Long userId) {
        lendService.lendBook(bookId, userId);
    }

    @RequestMapping(value = "/due", method = RequestMethod.PATCH, consumes = "application/json")
    public void returnBook(@RequestParam(value = "bookId") Long bookId) {
        lendService.returnBook(bookId);
    }

}
