package com.gft.library.OperationManagement.model;

import com.gft.library.BookInventory.model.Book;
import com.gft.library.UserRegister.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "lend_history")
@Data
@NoArgsConstructor
public class LendHistory {

    @Id
    @GenericGenerator(
            name = "lend_sequence",
            strategy = "sequence",
            parameters = {
                    @org.hibernate.annotations.Parameter(
                            name = "sequence",
                            value = "lend_sequence"
                    )

            })
    @GeneratedValue(generator = "lend_sequence")
    @Column(name = "lend_id")
    private Long lendId;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = Book.class)
    @JoinColumn(name="book_id", referencedColumnName = "book_id")
    private Long bookId;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = User.class)
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    private Long userId;

    @Column(name = "lend_date")
    private Date lendDate;

    @Column(name = "return_date")
    private Date returnDate;
}
