package com.gft.library.OperationManagement.service;

public interface LendService {

    void lendBook(Long bookId, Long userId);
    void returnBook(Long bookId);
}
