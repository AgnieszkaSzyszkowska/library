package com.gft.library.OperationManagement.service.Impl;

import com.gft.library.OperationManagement.model.LendHistory;
import com.gft.library.OperationManagement.repository.LendRepository;
import com.gft.library.OperationManagement.service.LendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class LendServiceDB implements LendService {

    private LendRepository lendRepository;

    @Autowired
    public LendServiceDB(final LendRepository lendRepository) {
        this.lendRepository = lendRepository;
    }

    @Override
    public void lendBook(Long bookId, Long userId) {
        LendHistory lendHistory = new LendHistory();
        lendHistory.setBookId(bookId);
        lendHistory.setUserId(userId);
        Date currentDate = new Date (new java.util.Date().getTime());
        lendHistory.setLendDate(currentDate);
        lendRepository.save(lendHistory);
    }

    @Override
    public void returnBook(Long bookId) {
        Date currentDate = new Date (new java.util.Date().getTime());
        //lendRepository.returnBook(bookId, currentDate);
    }

}
