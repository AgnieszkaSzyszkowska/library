package com.gft.library.UserRegister.mapper;

import com.gft.library.UserRegister.service.dto.UserDTO;
import com.gft.library.UserRegister.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User mapUserDtoToUserDao(UserDTO userDTO){
        User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setUserSurname(userDTO.getUserSurname());
        user.setUserRole(userDTO.getUserRole());

        return user;
    }

    public UserDTO mapUserDaoToUserDto(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(user.getUserId());
        userDTO.setUserName(user.getUserName());
        userDTO.setUserSurname(user.getUserSurname());
        userDTO.setUserRole(user.getUserRole());

        return userDTO;
    }
}
