package com.gft.library.UserRegister.controller;

import com.gft.library.UserRegister.service.UserService;
import com.gft.library.UserRegister.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = "application/json")
    public void addUser(@RequestBody UserDTO userDTO) {
        userService.create(userDTO);
    }

    @RequestMapping(value = "/modify", method = RequestMethod.PATCH, consumes = "application/json")
    public void modifyUser(@RequestBody UserDTO userDTO) {
        userService.update(userDTO);
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET, produces = "application/json")
    public UserDTO findUser(@RequestParam(value = "id") Long id) {
        return userService.findById(id);
    }

    @RequestMapping(value = "/name", method = RequestMethod.GET, produces = "application/json")
    public List<UserDTO> findUserByName(@RequestParam(value = "name") String name) {
        return userService.findByName(name);
    }

    @RequestMapping(value = "/surname", method = RequestMethod.GET, produces = "application/json")
    public List<UserDTO> findUserBySurname(@RequestParam(value = "surname") String surname) {
        return userService.findBySurname(surname);
    }
}
