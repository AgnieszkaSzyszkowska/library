package com.gft.library.UserRegister.model;

import com.gft.library.BookInventory.model.Book;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor
public class User {

    @Id
    @GenericGenerator(
            name = "sequence",
            strategy = "sequence",
            parameters = {
                    @org.hibernate.annotations.Parameter(
                            name = "sequence",
                            value = "sequence"
                    )

            })
    @GeneratedValue(generator = "sequence")
    @Column(name = "customer_id")
    private Long userId;

    @Column(name = "customer_name")
    private String userName;

    @Column(name = "customer_surname")
    private String userSurname;

    @Column(name = "customer_role")
    private String userRole;

    @OneToMany
    private List<Book> currentBooks;

}
