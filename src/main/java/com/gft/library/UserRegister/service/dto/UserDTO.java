package com.gft.library.UserRegister.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UserDTO {

    private Long userId;

    private String userName;

    private String userSurname;

    private String userRole;

}
