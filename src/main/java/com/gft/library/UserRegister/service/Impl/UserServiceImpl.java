package com.gft.library.UserRegister.service.Impl;

import com.gft.library.UserRegister.repository.UserRepository;
import com.gft.library.UserRegister.service.UserService;
import com.gft.library.UserRegister.service.dto.UserDTO;
import com.gft.library.UserRegister.model.User;
import com.gft.library.UserRegister.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserMapper userMapper;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository, final UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public void create(UserDTO userDTO) {
        User user = userMapper.mapUserDtoToUserDao(userDTO);
        userRepository.save(user);
    }

    @Override
    public void update(UserDTO userDTO) {

    }

    @Override
    public UserDTO findById(Long id) {
        return userMapper.mapUserDaoToUserDto(userRepository.findOne(id));
    }

    @Override
    public List<UserDTO> findByName(String name) {
        List<UserDTO> userDTOS = new ArrayList<>();
        List<User> users = userRepository.findByUserName(name);
        for(User u: users){
            userDTOS.add(userMapper.mapUserDaoToUserDto(u));
        }
        return userDTOS;
    }

    @Override
    public List<UserDTO> findBySurname(String surname) {
        List<UserDTO> userDTOS = new ArrayList<>();
        List<User> users = userRepository.findByUserSurname(surname);
        for(User u: users){
            userDTOS.add(userMapper.mapUserDaoToUserDto(u));
        }
        return userDTOS;
    }
}
