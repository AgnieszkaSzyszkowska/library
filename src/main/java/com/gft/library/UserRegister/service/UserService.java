package com.gft.library.UserRegister.service;

import com.gft.library.UserRegister.service.dto.UserDTO;

import java.util.List;

public interface UserService {

    void create(UserDTO userDTO);

    void update(UserDTO userDTO);

    UserDTO findById(Long id);

    List<UserDTO> findByName(String name);

    List<UserDTO> findBySurname(String surname);
}
